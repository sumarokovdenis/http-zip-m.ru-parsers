
from bs4 import BeautifulSoup
from requests import Session
import os
import shutil
from openpyxl import Workbook
from openpyxl.styles import Border
from openpyxl.styles import Side, Alignment

SITE_URL = 'http://zip-m.ru'
user_agent = ("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 "
              "(KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36")

login = '81399'
password = '25a30481'
login_url = 'http://zip-m.ru/scripts/check.php'

class Parser:

    def __init__(self):
        self.session = Session()
        self.catalog_dict = {}
        self.cards_dict = {}
        self.session.headers.update({'Accept-Encoding': 'gzip, deflate', 'User-Agent': user_agent})
        self.wb = Workbook()
        self.ws = self.wb.active

    def login(self):
        data = {
            'login': login,
            'x': 36,
            'y': 29,
            'password': password,
            'url': '/'
        }
        self.session.get(SITE_URL)
        self.session.post(login_url, data=data)

    def start_parse(self):

        self.login()
        response = self.session.get(SITE_URL)
        bs = BeautifulSoup(response.text, "html.parser")

        right_menu = bs.find('td', {'class': 'fright'})

        # collect catalog links and texts

        bwcs = right_menu.findAll('div', {'class': 'bwc'})
        bwcs = bwcs[0:2]
        for bwc in bwcs:
            a_s = bwc.findAll('a')
            for a in a_s:
                a_src = a.attrs.get('href')
                a_text = a.text
                self.catalog_dict.update({a_src: a_text})

        # find cards in catalog page

        side = Side(style='medium')
        border = Border(left=side, right=side,
                        top=side, bottom=side)
        column = 1
        row = 1

        for catalog_src in self.catalog_dict:
            try:
                tr_list = []

                catalog_full_url = SITE_URL + catalog_src
                req = self.session.get(catalog_full_url)
                bs_c = BeautifulSoup(req.text)

                tr1s = bs_c.findAll('tr', {'class': 't1'}, recursive=True)
                tr2s = bs_c.findAll('tr', {'class': 't2'}, recursive=True)
                trs = tr1s + tr2s

                tr_list.append(trs)

                paginator_p = bs_c.find('span', {'class': 'pgsel'})
                center_links = []

                if paginator_p:
                    paginator_td = paginator_p.parent
                    paginator_as = paginator_td.findAll('a')
                    for a in paginator_as:
                        center_links.append(a.attrs.get('href'))

                for link in center_links:
                    catalog_full_url = SITE_URL + link

                    request = self.session.get(catalog_full_url)
                    bs_cat = BeautifulSoup(request.text, 'html.parser')

                    tr1s = bs_cat.findAll('tr', {'class': 't1'}, recursive=True)
                    tr2s = bs_cat.findAll('tr', {'class': 't2'}, recursive=True)
                    trs = tr1s + tr2s

                    tr_list.append(trs)

                for trs in tr_list:

                    for tr in trs:
                        tds = tr.findAll('td')
                        code = tds[0].text
                        name = tds[2].text
                        group = tds[3].text
                        price = tds[4].text
                        stock = tds[5].text

                        img_a = tds[2].find('a')
                        row_list = [
                            self.catalog_dict[catalog_src],
                            code,
                            name,
                            group,
                            price,
                            stock,
                        ]
                        if img_a:

                            img_src = img_a.attrs.get('href')

                            img_file_name = os.path.basename(img_src)

                            img_bin = self.session.get((SITE_URL + img_src), stream=True)

                            img_name_path = './images/' + img_file_name

                            row_list.append(img_name_path)

                            with open(img_name_path, 'wb') as out_file:
                                shutil.copyfileobj(img_bin.raw, out_file)
                            del img_bin

                        for column_text in row_list:
                            new_cell = self.ws.cell(row=row, column=column)
                            new_cell.border = border
                            new_cell.value = column_text
                            column += 1

                        column = 1
                        row += 1

            except Exception as e:
                print('sec exception', e)

        self.wb.save('result.xlsx')

if __name__ == '__main__':

    parser = Parser()
    parser.start_parse()
